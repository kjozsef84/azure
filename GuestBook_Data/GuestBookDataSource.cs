﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Azure;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace GuestBook_Data
{
    public class GuestBookDataSource
    {
        private static CloudStorageAccount storageAccount;

        static GuestBookDataSource()
        {
            storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("DataConnectionString"));

            CloudTableClient cloudTableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = cloudTableClient.GetTableReference("GuestBookEntry");
            table.CreateIfNotExists();

        }

        public GuestBookDataSource()
        {

        }

        public IEnumerable<GuestBookEntry> GetGuestBookEntries()
        {
            CloudTableClient cloudTableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = cloudTableClient.GetTableReference("GuestBookEntry");

            TableQuery<GuestBookEntry> query = (new TableQuery<GuestBookEntry>()).Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, DateTime.UtcNow.ToString("MMddyyyy")));
            var result = table.ExecuteQuery(query).ToList<GuestBookEntry>();
            return result;
        }

        public void AddGuestBookEntry(GuestBookEntry newItem)
        {
            CloudTable table = storageAccount.CreateCloudTableClient().GetTableReference("GuestBookEntry");
            table.Execute(TableOperation.Insert(newItem));
        }

        public void UpdateImageThumbnail(string partitionKey, string rowKey, string thumbUrl)
        {
            CloudTable table = storageAccount.CreateCloudTableClient().GetTableReference("GuestBookEntry");
            GuestBookEntry entry = table.Execute(TableOperation.Retrieve<GuestBookEntry>(partitionKey, rowKey)).Result as GuestBookEntry;

            entry.ThumbnailUrl = thumbUrl;
            table.Execute(TableOperation.Replace(entry));
        }
    }
}
